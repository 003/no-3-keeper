package craky.keeper.client;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;
import craky.keeper.view.KeeperListView;

public class TitleTabSwitcher implements CompoundButton.OnCheckedChangeListener
{
    private KeeperActivity activity;

    private RadioButton btnPay, btnIncome;

    private KeeperListView payList, incomeList;

    private TextView emptyView;

    private PullDownListener pullDownListener;

    public TitleTabSwitcher(KeeperActivity activity, RadioButton btnPay, RadioButton btnIncome, KeeperListView payList,
                    KeeperListView incomeList, TextView emptyView, PullDownListener pullDownListener)
    {
        this.activity = activity;
        this.btnPay = btnPay;
        this.btnIncome = btnIncome;
        this.payList = payList;
        this.incomeList = incomeList;
        this.emptyView = emptyView;
        this.pullDownListener = pullDownListener;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
    {
        KeeperListView currentList = null, anotherLlist = null;

        if(isChecked && buttonView == btnPay)
        {
            currentList = payList;
            anotherLlist = incomeList;
        }
        else if(isChecked && buttonView == btnIncome)
        {
            currentList = incomeList;
            anotherLlist = payList;
        }

        if(currentList != null)
        {
            int emptyInfoId = (currentList == payList)? R.string.no_pay: R.string.no_income;
            emptyView.setText(activity.getResources().getString(emptyInfoId));
            anotherLlist.cancelLoad(true);
            anotherLlist.cancelLoadMore();

            if(pullDownListener != null)
            {
                pullDownListener.resetHeader();
            }

            if(currentList.getCount() > currentList.getFooterViewsCount())
            {
                currentList.setVisibility(View.VISIBLE);
            }
            else
            {
                emptyView.setVisibility(View.VISIBLE);
            }

            anotherLlist.setVisibility(View.GONE);

            if(!currentList.isFirstFinished())
            {
                currentList.loadDatasFirst();
            }
        }
    }
}