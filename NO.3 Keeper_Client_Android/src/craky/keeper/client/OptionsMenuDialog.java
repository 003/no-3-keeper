package craky.keeper.client;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;

public class OptionsMenuDialog extends DialogFragment implements OnClickListener, OnKeyListener
{
    private KeeperActivity context;

    private Dialog dialog;

    private Button btnSwitchStatusBar;

    public OptionsMenuDialog(KeeperActivity context)
    {
        this.context = context;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        if(dialog == null)
        {
            dialog = new Dialog(context, R.style.MenuDialog);
            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.gravity = Gravity.FILL_HORIZONTAL | Gravity.BOTTOM;
            Point screenSize = new Point();
            context.getWindowManager().getDefaultDisplay().getSize(screenSize);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setContentView(R.layout.options_menu);
            dialog.setOnKeyListener(this);
            btnSwitchStatusBar = (Button)dialog.findViewById(R.id.btnSwitchStatusBar);
            btnSwitchStatusBar.setMinimumWidth(screenSize.x);
            btnSwitchStatusBar.setOnClickListener(this);
        }

        context.switchMenuButtonText(btnSwitchStatusBar);
        return dialog;
    }

    public void onClick(View v)
    {
        context.switchStatusBar();
        context.onOptionsMenuClosed(null);
    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event)
    {
        if(keyCode == KeyEvent.KEYCODE_MENU && event.getAction() == KeyEvent.ACTION_UP && !event.isCanceled())
        {
            context.onOptionsMenuClosed(null);
            return true;
        }

        return false;
    }
}