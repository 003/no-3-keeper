package craky.keeper.bean;

import java.io.Serializable;
import java.util.List;

public class ResultSet implements Serializable
{
    private static final long serialVersionUID = -7066503307101100238L;

    private User user;

    private List<Category> payCategoryList, incomeCategoryList;

    private List<Pay> payList;

    private List<Income> incomeList;

    private Pay pay;

    private Income income;

    private long lastUpdateTime, oldUpdateTime;

    public User getUser()
    {
        return this.user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public List<Category> getPayCategoryList()
    {
        return this.payCategoryList;
    }

    public void setPayCategoryList(List<Category> payCategoryList)
    {
        this.payCategoryList = payCategoryList;
    }

    public List<Category> getIncomeCategoryList()
    {
        return this.incomeCategoryList;
    }

    public void setIncomeCategoryList(List<Category> incomeCategoryList)
    {
        this.incomeCategoryList = incomeCategoryList;
    }

    public List<Pay> getPayList()
    {
        return this.payList;
    }

    public void setPayList(List<Pay> payList)
    {
        this.payList = payList;
    }

    public List<Income> getIncomeList()
    {
        return this.incomeList;
    }

    public void setIncomeList(List<Income> incomeList)
    {
        this.incomeList = incomeList;
    }

    public Pay getPay()
    {
        return this.pay;
    }

    public void setPay(Pay pay)
    {
        this.pay = pay;
    }

    public Income getIncome()
    {
        return this.income;
    }

    public void setIncome(Income income)
    {
        this.income = income;
    }

    public long getLastUpdateTime()
    {
        return this.lastUpdateTime;
    }

    public void setLastUpdateTime(long lastUpdateTime)
    {
        this.lastUpdateTime = lastUpdateTime;
    }

    public long getOldUpdateTime()
    {
        return this.oldUpdateTime;
    }

    public void setOldUpdateTime(long oldUpdateTime)
    {
        this.oldUpdateTime = oldUpdateTime;
    }
}