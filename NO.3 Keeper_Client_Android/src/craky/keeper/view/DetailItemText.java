package craky.keeper.view;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;

public class DetailItemText extends ClearableEditText
{
    public DetailItemText(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public void setReadonly()
    {
        this.setCursorVisible(false);
        this.setKeyListener(null);
    }

    public void setMultiLine()
    {
        this.setSingleLine(false);
        this.setMinLines(1);
        this.setMaxLines(5);
        this.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
    }
}