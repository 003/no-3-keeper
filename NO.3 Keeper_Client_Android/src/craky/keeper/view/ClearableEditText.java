package craky.keeper.view;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import craky.keeper.client.R;
import craky.keeper.util.KeeperUtil;

public class ClearableEditText extends EditText
{
    private Drawable imgClear;

    public ClearableEditText(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.post(new Runnable()
        {
            public void run()
            {
                imgClear = getResources().getDrawable(R.drawable.clear_text);
                int imageSize = (int)(19 * KeeperUtil.density);
                int textImageGap = getTextImageGap();
                imgClear.setBounds(textImageGap, 0, imageSize + textImageGap, imageSize);
                imgClear.setAlpha(73);
                resetRightDrawable();
            }
        });
        this.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            public void onFocusChange(View v, boolean hasFocus)
            {
                resetRightDrawable();
            }
        });
        this.addTextChangedListener(new TextWatcher()
        {
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {}

            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {}

            public void afterTextChanged(Editable s)
            {
                resetRightDrawable();
            }
        });
        this.setOnTouchListener(new View.OnTouchListener()
        {
            public boolean onTouch(View v, MotionEvent event)
            {
                if(event.getAction() == MotionEvent.ACTION_UP)
                {
                    clearTextAction((int)event.getX(), (int)event.getY());
                }

                return false;
            }
        });
    }

    private void resetRightDrawable()
    {
        boolean hasRightDrawable = this.isEnabled() && this.hasFocus() && this.getKeyListener() != null && this.getText().length() > 0;
        setCompoundDrawables(null, null, hasRightDrawable? imgClear: null, null);
    }

    private void clearTextAction(int x, int y)
    {
        if(this.getCompoundDrawables()[2] == null)
        {
            return;
        }

        Rect rect = imgClear.getBounds();
        int imageWidth = rect.width();
        int imageHeight = rect.height();
        int right = this.getMeasuredWidth() - this.getPaddingRight();
        int left = right - imageWidth;
        int top = (this.getMeasuredHeight() - imageHeight) / 2;
        int bottom = top + imageHeight;
        rect.set(left, top, right, bottom);

        if(rect.contains(x, y))
        {
            this.setText(null);
        }
    }

    protected int getTextImageGap()
    {
        return 0;
    }
}