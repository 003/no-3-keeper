package craky.keeper.sql;

public class NotInExpression extends CollectionExpression
{
    private static final long serialVersionUID = 5603012980883884272L;

    public NotInExpression(String columnName, Object[] values)
    {
        super(columnName, values);
    }

    protected String getOperation()
    {
        return "not in";
    }
}