@set CLASSPATH=lib\KeeperClient.jar;lib\hessian-4.0.37.jar;lib\SwingC.jar;
@if exist jre (goto start1) else (goto start2)
:start1
@start jre\bin\javaw -splash:image\splash.png craky.keeper.KeeperApp
@exit
:start2
@start javaw -splash:image\splash.png craky.keeper.KeeperApp
@exit