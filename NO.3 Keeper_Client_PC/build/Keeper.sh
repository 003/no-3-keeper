#!/bin/sh
CLASSPATH=./lib/SwingC.jar:./lib/hessian-4.0.37.jar:./lib/KeeperClient.jar
SPLASH=./image/splash.png
java -classpath $CLASSPATH -splash:$SPLASH craky.keeper.KeeperApp
